export interface CellContents {
  letter: string | null;
  type: 'free' | 'word' | 'random';
  direction?: string;
}

interface Place {
  row: number;
  column: number;
  direction: string;
}

interface Direction {
  rowDelta: 0 | 1;
  colDelta: 0 | 1;
}

const DIRECTIONS: { [key: string]: Direction } = {
  down: {rowDelta: 1, colDelta: 0},
  right: {rowDelta: 0, colDelta: 1},
  downRight: {rowDelta: 1, colDelta: 1},
};

const DIRECTION_NAMES = Object.keys(DIRECTIONS);


export class GridComputer {

  private grid: CellContents[][];
  private words: string[];
  private columns: number;
  private rows: number;

  constructor(columns: number, rows: number, wordListAsString: string) {
    this.columns = columns;
    this.rows = rows;
    this.grid = new Array(rows);
    for (let i = 0; i < rows; i++) {
      this.grid[i] = new Array(columns);
      for (let j = 0; j < columns; j++) {
        this.grid[i][j] = {letter: null, type: 'free'};
      }
    }

    this.words = wordListAsString.split(/\s/).map((word) => word.trim());
  }

  public fillGrid() {
    this.words.forEach((word) => {
      this.insertWordIntoGrid(word);
    });

  }

  public getGrid() {
    return this.grid;
  }

  public fillEmptyCells() {
    for (let row = 0; row < this.rows; row++) {
      for (let column = 0; column < this.columns; column++) {
        if (this.grid[row][column].type === 'free') {
          this.grid[row][column] = {letter: this.randomChar(), type: 'random'};
        }
      }
    }
  }

  public getWords() {
    return this.words;
  }

  private insertWordIntoGrid(word: string): void {
    const places: Place[] = this.findPossiblePlaces(word);
    if (places.length === 0) {
      throw new Error(`"${word}" does not fit into grid`);
    }
    const selectedPlace = Math.floor(Math.random() * places.length);
    this.insertWordIntoPlace(word, places[selectedPlace]);

  }

  private findPossiblePlaces(word: string): Place[] {
    const result: Place[] = [];
    DIRECTION_NAMES.forEach((direction) => {
      for (let row = 0; row < this.rows; row++) {
        for (let column = 0; column < this.columns; column++) {
          const place: Place = {row, column, direction};
          if (this.wordFitsIntoPlace(word, place)) {
            result.push(place);
          }
        }
      }
    });
    return result;
  }

  private wordFitsIntoPlace(word: string, place: Place) {
    for (let i = 0; i < word.length; i++) {
      if (this.isOccupied(place, i)) {
        return false;
      }
    }
    return true;
  }

  private isOccupied(place: Place, counter: number) {
    const delta = DIRECTIONS[place.direction];
    const checkRow = place.row + counter * delta.rowDelta;
    const checkColumn = place.column + counter * delta.colDelta;
    if (checkRow >= this.rows || checkColumn >= this.columns) {
      return true;
    }
    if (this.grid[checkRow][checkColumn].type !== 'free') {
      return true;
    }
  }

  private insertWordIntoPlace(word: string, place: Place) {
    for (let i = 0; i < word.length; i++) {
      const delta = DIRECTIONS[place.direction];
      const row = place.row + i * delta.rowDelta;
      const column = place.column + i * delta.colDelta;
      const letter = word[i];
      this.grid[row][column] = {letter, type: 'word', direction: place.direction};
    }
  }

  private randomChar(): string {
    const randomUpTo26 = Math.floor(Math.random() * 26);
    const charCode = randomUpTo26 + 65;
    return String.fromCharCode(charCode);
  }
}
