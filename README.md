# hidden-words

This is a quick-and-dirty hack to create riddles of "hidden words" for children (words hidden in random-letters).

see http://nknapp.gitlab.io/hidden-words

Please don't judge the code quality. It **is** quick and dirty. I hacked it down in an afternoon and I don't intend to
make it better. There are no tests and no docs. And some remains of the vue-cli starter are still there.


 

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
